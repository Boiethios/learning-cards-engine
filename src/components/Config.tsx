import * as React from 'react';
import { useEffect, useReducer } from 'react';
import * as Router from 'react-router-dom';

import { extract } from '../tar';
import { throwMsg, promiseAllSettled } from '../util';
import * as toml from 'toml';
import * as store from '../store';
import { Config, TomlManifest } from '../model';

interface State {
  logs: Log[];
  config: Config;
  topics: string[];
  audiosCorrect: string[];
  audiosWrong: string[];
}

//TODO add a cleanup to remove the orphan file if there has been an asset update.
type Action =
  | { type: 'log'; lines: Log[] }
  | { type: 'updateConfig'; key: string; value: any }
  | { type: 'addTopicAsset'; topic: string | undefined; fileBuf: Buffer; asset: string }
  | { type: 'addAudioCorrect'; added: string[]; errored: string[] }
  | { type: 'removeAudioCorrect'; name: string }
  | { type: 'addAudioWrong'; added: string[]; errored: string[] }
  | { type: 'removeAudioWrong'; name: string }
  | { type: 'wipeOut' };

function initializer(): State {
  return {
    logs: [],
    config: store.config.get(),
    topics: store.listTopics(),
    audiosCorrect: store.audiosCorrect.getAllFileNames(),
    audiosWrong: store.audiosWrong.getAllFileNames(),
  };
}

function reducer(state: State, action: Action): State {
  console.debug('Dispatched:', action);

  switch (action.type) {
    case 'log':
      return { ...state, logs: state.logs.concat(action.lines) };

    case 'updateConfig':
      const config = { ...state.config, [action.key]: action.value };
      store.config.set(config);
      return { ...state, config };

    case 'addTopicAsset':
      const { topic, fileBuf, asset } = action;
      if (asset.at(-1) === '/') {
        return { ...state, logs: state.logs.concat(Log.warn(`Skipped directory: ${asset}`)) };
      } else if (asset.endsWith('manifest.toml')) {
        const manifest = toml.parse(fileBuf.toString('utf-8'));
        for (const { name, picture, alt, audio, cards } of manifest.topics) {
          store.topic(name).setManifest({ description: { picture, alt, audio }, cards });
        }
        return { ...state, logs: state.logs.concat(Log.log('Read manifest')) };
      } else if (topic) {
        store.topic(topic).asset(asset).set(fileBuf);
        return {
          ...state,
          topics: [...new Set(state.topics.concat(topic))],
          logs: state.logs.concat(Log.log(`Added: ${asset}`)),
        };
      } else {
        return { ...state, logs: state.logs.concat(Log.warn(`Skipped orphan file: ${asset}`)) };
      }

    case 'addAudioCorrect':
      return {
        ...state,
        audiosCorrect: [...new Set(state.audiosCorrect.concat(action.added))],
        logs: state.logs.concat(action.errored.map((e) => Log.err(`Failed to store audio: ${e}`))),
      };

    case 'removeAudioCorrect':
      return { ...state, audiosCorrect: state.audiosCorrect.filter((n) => n !== action.name) };

    case 'addAudioWrong':
      return {
        ...state,
        audiosWrong: [...new Set(state.audiosWrong.concat(action.added))],
        logs: state.logs.concat(action.errored.map((e) => Log.err(`Failed to store audio: ${e}`))),
      };

    case 'removeAudioWrong':
      return { ...state, audiosWrong: state.audiosWrong.filter((n) => n !== action.name) };

    case 'wipeOut':
      return { ...state, audiosCorrect: [], audiosWrong: [], topics: [] };
  }
}

export function Config() {
  const navigate = Router.useNavigate();
  const [state, dispatch] = useReducer(reducer, undefined, initializer);

  useEffect(() => store.config.set(state.config), [state.config]);
  useEffect(
    () => document.getElementById('extract-logs')?.lastElementChild?.scrollIntoView(),
    [state.logs],
  );

  async function archiveChosen(event: React.ChangeEvent<HTMLInputElement>) {
    const file = event.target.files?.item(0) ?? throwMsg('Failed to choose the archive');

    try {
      // Get the manifest first:
      const topicsToAdd = await findUnique(
        extract(file.stream()),
        ({ header }) => header.name === 'manifest.toml',
      ).then(({ buffer }) => (toml.parse(buffer.toString('utf-8')) as TomlManifest).topics);

      // Remove first the topics that will be added:
      //newTopics.forEach((t: any) => store.topic(t.name).removeAll());

      for await (const { buffer, header } of extract(file.stream())) {
        const topic = topicsToAdd.find(
          (t) =>
            t.cards.some((c) => header.name == c.audio || header.name == c.picture) ||
            t.picture === header.name ||
            t.audio === header.name,
        );

        dispatch({
          type: 'addTopicAsset',
          asset: header.name,
          fileBuf: buffer,
          topic: topic?.name,
        });
      }

      dispatch({ type: 'log', lines: [Log.ok('Succeded to extract'), Log.end()] });
    } catch (e: any) {
      dispatch({ type: 'log', lines: [Log.err(`Failed to extract: ${e.msg}`), Log.end()] });
    }
  }

  async function addAudioCorrect(event: React.ChangeEvent<HTMLInputElement>) {
    const { fulfilled, rejected } = await promiseAllSettled(
      Array.from(event.target.files ?? throwMsg('Failed to get the audio file(s)')).map((f) =>
        store.audiosCorrect.set(f).then(() => f.name),
      ),
    );

    dispatch({ type: 'addAudioCorrect', added: fulfilled, errored: rejected });
  }

  const removeAudioCorrect = (name: string) => () => {
    store.audiosCorrect.remove(name);
    dispatch({ type: 'removeAudioCorrect', name });
  };

  async function addAudioWrong(event: React.ChangeEvent<HTMLInputElement>) {
    const { fulfilled, rejected } = await promiseAllSettled(
      Array.from(event.target.files ?? throwMsg('Failed to get the audio file(s)')).map((f) =>
        store.audiosWrong.set(f).then(() => f.name),
      ),
    );

    dispatch({ type: 'addAudioWrong', added: fulfilled, errored: rejected });
  }

  const removeAudioWrong = (name: string) => () => {
    store.audiosWrong.remove(name);
    dispatch({ type: 'removeAudioWrong', name });
  };

  function removeEverything() {
    if (confirm('Are you sure you want to empty the storage?')) {
      store.removeEverything();
      dispatch({ type: 'wipeOut' });
    }
  }

  const updateConfig = <K extends keyof Config>(key: K, value: Config[K]) =>
    dispatch({
      type: 'updateConfig',
      key,
      value,
    });

  return (
    <div className="page config">
      <div className="main-config">
        <div className="main-links">
          <a onClick={() => navigate('/')} href="">
            Go to main menu
          </a>
          <a onClick={removeEverything} href="">
            Empty storage
          </a>
        </div>

        <section className="config">
          <fieldset>
            <legend>Configuration</legend>
            <label>
              <input
                type="checkbox"
                checked={state.config.displayTextInSwiper}
                onChange={(e) => updateConfig('displayTextInSwiper', e.target.checked)}
              />
              Display the images label in the cards swiper
            </label>
            <label>
              <input
                type="checkbox"
                checked={state.config.enableConfigShortcut}
                onChange={(e) => updateConfig('enableConfigShortcut', e.target.checked)}
              />
              Enable the shortcut (4 taps/clicks) to go to this page from the menu
            </label>
          </fieldset>
        </section>

        <section className="topics">
          <h2>Topics</h2>
          <ul>
            {state.topics.map((topic) => (
              <li key={topic}>{topic}</li>
            ))}
          </ul>

          <input
            type="file"
            name="Assets Archive"
            accept="application/x-tar"
            onChange={archiveChosen}
          />
        </section>

        <section className="correct">
          <h2>Audio for the Correct Answers</h2>
          <ul>
            {state.audiosCorrect.map((fileName) => (
              <li key={fileName}>
                {fileName} (
                <a onClick={removeAudioCorrect(fileName)} href="">
                  remove
                </a>
                )
              </li>
            ))}
          </ul>

          <input
            type="file"
            name="Correct Answer Audio"
            accept="audio/*"
            multiple={true}
            onChange={addAudioCorrect}
          />
        </section>

        <section className="wrong">
          <h2>Audio for the Wrong Answers</h2>
          <ul>
            {state.audiosWrong.map((fileName) => (
              <li key={fileName}>
                {fileName} (
                <a onClick={removeAudioWrong(fileName)} href="">
                  remove
                </a>
                )
              </li>
            ))}
          </ul>

          <input
            type="file"
            name="Wrong Answer Audio"
            accept="audio/*"
            multiple={true}
            onChange={addAudioWrong}
          />
        </section>
      </div>

      <aside className="logs">
        <ul id="extract-logs">{Log.render(state.logs)}</ul>
      </aside>
    </div>
  );
}

type Log =
  | { type: 'log'; msg: string }
  | { type: 'warn'; msg: string }
  | { type: 'err'; msg: string }
  | { type: 'ok'; msg: string }
  | { type: 'end' };

const Log = {
  log: (msg: string): Log => ({ type: 'log', msg }),
  warn: (msg: string): Log => ({ type: 'warn', msg }),
  err: (msg: string): Log => ({ type: 'err', msg }),
  ok: (msg: string): Log => ({ type: 'ok', msg }),
  end: (): Log => ({ type: 'end' }),

  render: (lines: Log[]) =>
    lines.map((line, i) =>
      line.type === 'end' ? (
        <li key={i}>
          <hr />
        </li>
      ) : (
        <li key={i} className={line.type}>
          {line.msg}
        </li>
      ),
    ),
};

async function findUnique<T>(
  iterable: AsyncIterable<T>,
  predicate: (item: T) => boolean,
): Promise<T> {
  const matchingItems: T[] = [];
  for await (const item of iterable) {
    if (predicate(item)) {
      if (matchingItems.length !== 0) {
        throw new Error('There must be only manifest at the root of the archive.');
      }
      matchingItems.push(item);
    }
  }

  return matchingItems?.at(0) ?? throwMsg('There must be one manifest at the root of the archive.');
}
