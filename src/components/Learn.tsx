import { useParams } from 'react-router-dom';

import { useEffect, useReducer } from 'react';
import * as swiper from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css';

import * as store from '../store';
import { throwMsg } from '../util';

interface State {
  displayTextInSwiper: boolean;
  cards: { audio: HTMLAudioElement; picture: string; alt: string }[];
  currentCard: number;
}

type Action = { type: 'swiped'; index: number };

function initializer(params: any): State {
  const topic = store.topic(params.topic ?? throwMsg('No topic specified in the params'));
  const { displayTextInSwiper } = store.config.get();
  const cards = topic.getCards().map(({ audio, picture, alt }) => ({
    audio: new Audio(topic.asset(audio).get()),
    picture: topic.asset(picture).get(),
    alt,
  }));

  return {
    displayTextInSwiper,
    cards,
    currentCard: 0,
  };
}

function reducer(state: State, action: Action): State {
  console.debug('Dispatched:', action);

  switch (action.type) {
    case 'swiped':
      return { ...state, currentCard: action.index };
  }
}

export function Learn() {
  const [state, dispatch] = useReducer(reducer, useParams(), initializer);
  useEffect(() => runAudio(), [state.currentCard]);

  function runAudio() {
    const audio = state.cards[state.currentCard].audio;

    audio.currentTime = 0;
    audio.play();
  }

  function onSwipe({ realIndex }: swiper.Swiper) {
    state.cards[state.currentCard].audio.pause();
    dispatch({ type: 'swiped', index: realIndex });
  }

  function renderImage({ picture, alt }: { picture: string; alt: string }) {
    if (state.displayTextInSwiper) {
      return (
        <figure>
          <img src={picture} />
          <figcaption>{alt}</figcaption>
        </figure>
      );
    } else {
      return <img src={picture} alt={alt} />;
    }
  }

  return (
    <Swiper
      className="page learn"
      pagination={true}
      mousewheel={{ invert: true }}
      keyboard={{ enabled: true }}
      loop={true}
      onClick={() => runAudio()}
      onInit={() => runAudio()}
      onRealIndexChange={onSwipe}
    >
      {state.cards.map((card, i) => (
        <SwiperSlide key={i}>{renderImage(card)}</SwiperSlide>
      ))}
    </Swiper>
  );
}
