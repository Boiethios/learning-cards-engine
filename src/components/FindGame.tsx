import { useParams } from 'react-router-dom';
import { useEffect, useReducer } from 'react';

import weighted from 'weighted';

import * as store from '../store';
import { classes, throwMsg, playAudio, updateElem } from '../util';

interface State {
  audiosCorrect: HTMLAudioElement[];
  audiosWrong: HTMLAudioElement[];
  deck: { audio: string; picture: string; alt: string; index: number }[];
  weights: number[];
  cards: { picture: string; alt: string; correct: boolean; clicked: boolean }[];
  audio: HTMLAudioElement;
  actionEnabled: boolean;
}

type Action =
  | { type: 'card-clicked'; index: number }
  | { type: 'new-random-cards' }
  | { type: 'enable-action' };

function initializer(params: any): State {
  const topic = store.topic(params.topic ?? throwMsg('No topic specified in the params'));
  const deck = topic.getCards().map(({ audio, picture, alt }, index) => ({
    index,
    picture: topic.asset(picture).get(),
    audio: topic.asset(audio).get(),
    alt,
  }));

  return reducer(
    {
      audiosCorrect: store.audiosCorrect.getAllAudios().map((uri) => new Audio(uri)),
      audiosWrong: store.audiosWrong.getAllAudios().map((uri) => new Audio(uri)),
      deck,
      weights: Array(deck.length).fill(1),
      cards: [],
      audio: new Audio(),
      actionEnabled: true,
    },
    { type: 'new-random-cards' },
  );
}

function reducer(state: State, action: Action): State {
  console.debug('Dispatched:', action);

  switch (action.type) {
    case 'card-clicked':
      const cards_ = updateElem(state.cards, action.index, (elem) => ({ ...elem, clicked: true }));

      return { ...state, cards: cards_, actionEnabled: false };

    case 'new-random-cards':
      // Choose one card (the correct one):
      const chosen = weighted.select(state.deck, state.weights);
      const audio = new Audio(chosen.audio);

      // Make all the cards more likely to be chosen, but the one chosen just now:
      const weights = state.weights.map((w) => (w == 0 ? 1 : w * 2));
      weights[chosen.index] = 0;

      // Then choose three other cards:
      const newCards = [
        ...getSample(state.deck, 4)
          .filter((c) => c.index != chosen.index)
          .slice(undefined, 3),
        chosen,
      ].map(({ picture, alt, index }) => ({
        picture,
        alt,
        correct: index === chosen.index,
        clicked: false,
      }));

      return { ...state, weights, cards: shuffle(newCards), actionEnabled: false, audio };

    case 'enable-action':
      return { ...state, actionEnabled: true };
  }
}

export function FindGame() {
  const [state, dispatch] = useReducer(reducer, useParams(), initializer);
  useEffect(runAudio, [state.weights]);

  function runAudio() {
    //TODO: let the user click before the end of audio? Maybe with a callback min (1 sec, audio length)?
    playAudio(state.audio).then(() => dispatch({ type: 'enable-action' }));
  }

  async function onGuess(index: number) {
    dispatch({ type: 'card-clicked', index });

    if (state.cards[index].correct) {
      await playRandomAudio(state.audiosCorrect);
      dispatch({ type: 'new-random-cards' });
    } else {
      await playRandomAudio(state.audiosWrong);
      runAudio();
    }
  }

  const cardClickable = (state: State, index: number) =>
    state.actionEnabled && state.cards[index].clicked === false;
  const cardClass = (state: State, index: number) =>
    state.cards[index].clicked ? (state.cards[index].correct ? 'correct' : 'wrong') : undefined;

  if (state.deck.length < 4) {
    return <div>There are only {state.deck.length} cards for this topic, the game cannot run.</div>;
  }

  return (
    <div className="page find-game">
      {state.cards.map((c, i) => (
        <div
          key={i}
          className={'image_' + i}
          onClick={cardClickable(state, i) ? () => onGuess(i) : undefined}
        >
          <div className={cardClass(state, i)}></div>
          <img
            className={classes({
              card: true,
              clickable: state.actionEnabled,
              dimed: state.cards[i].correct === false && state.cards[i].clicked,
            })}
            src={c.picture}
            alt={c.alt}
          />
        </div>
      ))}

      <div className="play-audio-container">
        <div
          className={classes({
            'play-audio round-button card': true,
            clickable: state.actionEnabled,
          })}
          onClick={state.actionEnabled ? runAudio : undefined}
        >
          🔊
        </div>
      </div>
    </div>
  );
}

async function playRandomAudio(audios: HTMLAudioElement[]) {
  const randomAudio = randomElement(audios);
  if (randomAudio != null) {
    await playAudio(randomAudio);
  }
}

/**
 * Returns randomly a number of items from an array.
 * @param n The number of items to return.
 */
function getSample<T>(array: T[], n: number): T[] {
  const result: T[] = new Array(n);
  let len = array.length;
  const taken: number[] = new Array(len);

  while (n--) {
    var x = Math.floor(Math.random() * len);
    result[n] = array[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}

/**
 * Returns another array with the items in a different order.
 */
function shuffle<T>(array: T[]): T[] {
  let currentIndex = array.length;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {
    // Pick a remaining element.
    const randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
  }

  return array;
}

function randomElement<T>(array: T[]): T | undefined {
  return array.at(Math.floor(Math.random() * array.length));
}
