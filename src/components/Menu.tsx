import { useEffect, useReducer } from 'react';
import * as Router from 'react-router-dom';

import * as store from '../store';
import { Mode, TopicDescription } from '../model';
import { classes, playAudio } from '../util';

let memoryState: Mode = 'learn';

interface State {
  transition: { active: false } | { active: true; audio: string; name: string };
  mode: Mode;
  topics: (TopicDescription & { name: string })[];
  enableConfigShortcut: boolean;
  lastClick: number;
  countClick: number;
}

type Action =
  | { type: 'shift-mode' }
  | { type: 'clicked-anywhere'; when: number }
  | { type: 'topic-clicked'; audio: string; name: string };

function initializer(_params: any): State {
  const { enableConfigShortcut } = store.config.get();
  const topics = store.listTopics().map((topicName) => {
    const topic = store.topic(topicName);
    const { picture, audio, alt } = topic.getDescription();
    return {
      picture: topic.asset(picture).get(),
      audio: topic.asset(audio).get(),
      alt,
      name: topicName,
    };
  });

  return {
    transition: { active: false },
    mode: memoryState,
    topics,
    enableConfigShortcut,
    lastClick: new Date().getTime(),
    countClick: 0,
  };
}

function reducer(state: State, action: Action): State {
  console.debug('Dispatched:', action);

  switch (action.type) {
    case 'shift-mode':
      switch (state.mode) {
        case 'learn':
          return { ...state, mode: 'find-game' };
        case 'find-game':
          return { ...state, mode: 'learn' };
      }

    case 'clicked-anywhere':
      if (action.when - state.lastClick < 300) {
        return { ...state, countClick: state.countClick + 1, lastClick: action.when };
      } else {
        return { ...state, countClick: 1, lastClick: action.when };
      }

    case 'topic-clicked':
      return { ...state, transition: { active: true, ...action } };
  }
}

export function Menu() {
  const navigate = Router.useNavigate();
  const [state, dispatch] = useReducer(reducer, {}, initializer);

  useEffect(() => ((memoryState = state.mode), undefined), [state.mode]);
  useEffect(() => (state.countClick === 4 ? navigate('config') : undefined), [state.countClick]);
  useEffect(() => {
    const tr = state.transition;
    if (tr.active) {
      playAudio(new Audio(tr.audio)).then(() => navigate(`/${state.mode}/${tr.name}`));
    }
  }, [state.transition]);

  const onClickAnywhere = () =>
    state.enableConfigShortcut
      ? dispatch({ type: 'clicked-anywhere', when: Date.now() })
      : undefined;
  const onModeSwitch = () => dispatch({ type: 'shift-mode' });
  const onTopicClick = (audio: string, name: string) => () =>
    dispatch({ type: 'topic-clicked', audio, name });

  //TODO optimize when it's done:
  //React.useEffect(() => );

  return (
    <div
      className={`page menu mode-${state.mode}`}
      onClick={state.transition.active ? undefined : onClickAnywhere}
    >
      {state.topics.length === 0 ? (
        <a href="" onClick={() => navigate('config')}>
          No topic has been added
        </a>
      ) : (
        state.topics.map(({ picture, alt, name, audio }) => (
          <div
            className={classes({
              'topic card': true,
              clickable: state.transition.active === false,
            })}
            onClick={state.transition.active ? undefined : onTopicClick(audio, name)}
            key={name}
          >
            <img src={picture} alt={alt} />
          </div>
        ))
      )}

      <div id="mode-switch" className="card round-button clickable" onClick={onModeSwitch}>
        ↝
      </div>
    </div>
  );
}
