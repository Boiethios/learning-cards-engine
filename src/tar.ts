import * as tar from 'tar-stream';
import { ReadableWebToNodeStream } from 'readable-web-to-node-stream';
import { EventIterator } from 'event-iterator';

/**
 * Extract the files from a tar archive, inside the browser.
 */
export function extract(
  stream: ReadableStream<Uint8Array>,
): AsyncIterable<{ buffer: Buffer; header: tar.Headers }> {
  return new EventIterator(({ push, stop }) => {
    const extract = tar
      .extract()
      .on('entry', (header, stream, next) => {
        readStream(stream).then((buffer) => push({ buffer, header }));

        next(); // ready for next entry
      })
      .on('finish', stop);

    new ReadableWebToNodeStream(stream).pipe(extract);
  });
}

/**
 * Reads the tar-stream file stream in memory as a Buffer.
 */
function readStream(stream: any /* internal tar-stream type */): Promise<Buffer> {
  return new Promise((resolve) => {
    const chunks: Buffer[] = [];
    stream.on('data', (chunk: Buffer) => chunks.push(chunk));
    stream.on('end', () => resolve(concatArrays(chunks)));
  });
}

/**
 * Concat an array of `Buffer`.
 */
function concatArrays(arrays: Buffer[]): Buffer {
  let totalLength = arrays.reduce((acc, value) => acc + value.length, 0);

  let result = Buffer.alloc(totalLength);

  let length = 0;
  for (let array of arrays) {
    result.set(array, length);
    length += array.length;
  }

  return result;
}
