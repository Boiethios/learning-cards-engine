/**
 * Creates a storage that works with paths instead of plain keys.
 */
export const storage = (separator: string) => ({
  /**
   * Get the data at the given path.
   */
  get(path: string[]): string | null {
    return localStorage.getItem(path.join(separator));
  },

  /**
   * Get the data at the given path. Throws if it does not exist.
   */
  getOrThrow(path: string[]): string {
    const result = localStorage.getItem(path.join(separator));

    if (result == null) {
      throw new Error(`Cannot find the path "${path.join(separator)}" in the storage`);
    }
    return result;
  },

  /**
   * Set the data at the given path.
   */
  set(value: string, path: string[]) {
    localStorage.setItem(path.join(separator), value);
  },

  /**
   * Get the data at the given path.
   */
  remove(path: string[]): boolean {
    const key = path.join(separator);
    if (localStorage.hasOwnProperty(key) === false) {
      console.warn('Failed to remove key that does not exist at path', path);
      return false;
    }
    localStorage.removeItem(key);
    return true;
  },

  /**
   * Removes all the data starting with the given path.
   */
  removeAll(startingPath: string[]) {
    const path = startingPath.map((s) => s + separator).join('');

    for (const key of Object.keys(localStorage).filter((k) => k.startsWith(path))) {
      localStorage.removeItem(key);
    }
  },

  /**
   * Returns all the [key, value] pairs of keys starting with the given path.
   */
  getPrefixedBy(startingPath: string[]): [string, string][] {
    const path = startingPath.map((s) => s + separator).join('');

    return Object.entries(localStorage).filter(([key]) => key.startsWith(path));
  },

  /**
   * Returns all keys matching the given regex.
   */
  match(regex: RegExp): RegExpMatchArray[] {
    return Object.keys(localStorage)
      .map((m) => m.match(regex))
      .filter((m): m is RegExpMatchArray => m != null);
  },
});
