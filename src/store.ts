import { throwMsg, readAsDataUri } from './util';
import { storage } from './pathedStorage';
import { Config, TopicDescription, Card, TopicManifest } from './model';

const ASSET_KEY = 'asset';
const CORRECT_AUDIO_KEY = 'correct';
const WRONG_AUDIO_KEY = 'wrong';
const SEPARATOR = '->';
const myStorage = storage(SEPARATOR);

export function listTopics(): string[] {
  const keys = myStorage
    .match(new RegExp(`^${ASSET_KEY}\\->(\\S+)\\->manifest\\.json$`))
    .map((a) => a.at(1));

  return [...new Set(keys)].filter((k): k is string => k != null);
}

export const topic = (topicName: string) => ({
  asset: (assetName: string) => ({
    set(data: Buffer) {
      const { binary, mime } = fileType(assetName);

      const content = binary ? data.toString('base64') : encodeURIComponent(data.toString('utf-8'));
      const dataUri = `data:${mime};${binary ? 'base64' : 'utf-8'},${content}`;
      myStorage.set(dataUri, [ASSET_KEY, topicName, assetName]);
    },
    get(): string {
      return myStorage.getOrThrow([ASSET_KEY, topicName, assetName]);
    },
  }),

  setManifest(manifest: TopicManifest) {
    myStorage.set(JSON.stringify(manifest), [ASSET_KEY, topicName, 'manifest.json']);
  },

  getDescription(): TopicDescription {
    return JSON.parse(myStorage.getOrThrow([ASSET_KEY, topicName, 'manifest.json'])).description;
  },

  getCards(): Card[] {
    return JSON.parse(myStorage.getOrThrow([ASSET_KEY, topicName, 'manifest.json'])).cards;
  },

  removeAll() {
    myStorage.removeAll([ASSET_KEY, topicName]);
  },
});

export const config = {
  get(): Config {
    return (
      JSON.parse(myStorage.get(['config']) as string) || {
        displayTextInSwiper: false,
        enableConfigShortcut: true,
      }
    );
  },

  set(config: Config) {
    myStorage.set(JSON.stringify(config), ['config']);
  },
};

export const audiosCorrect = {
  getAllAudios: (): string[] =>
    myStorage.getPrefixedBy([CORRECT_AUDIO_KEY]).map(([_, value]) => value),

  getAllFileNames: (): string[] =>
    myStorage
      .getPrefixedBy([CORRECT_AUDIO_KEY])
      .map(([key]) => key.slice(CORRECT_AUDIO_KEY.length + SEPARATOR.length)),

  async set(file: File) {
    const dataUri = await readAsDataUri(file);

    myStorage.set(dataUri, [CORRECT_AUDIO_KEY, file.name]);
  },

  remove(name: string) {
    myStorage.remove([CORRECT_AUDIO_KEY, name]);
  },
};

export const audiosWrong = {
  getAllAudios: (): string[] =>
    myStorage.getPrefixedBy([WRONG_AUDIO_KEY]).map(([_, value]) => value),

  getAllFileNames: (): string[] =>
    myStorage
      .getPrefixedBy([WRONG_AUDIO_KEY])
      .map(([key]) => key.slice(WRONG_AUDIO_KEY.length + SEPARATOR.length)),

  async set(file: File) {
    const dataUri = await readAsDataUri(file);

    myStorage.set(dataUri, [WRONG_AUDIO_KEY, file.name]);
  },

  remove(name: string) {
    myStorage.remove([WRONG_AUDIO_KEY, name]);
  },
};

/**
 * Wipes the whole storage out.
 */
export function removeEverything() {
  console.log('Emptying the storage');
  myStorage.removeAll([]);
}

/* UTIL */

/**
 * Crappy hack, but hey, it (mostly) works.
 */
function fileType(name: string): { binary: boolean; mime: string } {
  const ext = name.split('.').slice(-1).at(0) ?? throwMsg(`File ${name} must have an extension`);
  switch (ext) {
    case 'svg':
      return { binary: false, mime: `image/svg+xml` };

    case 'jpg':
    case 'jpeg':
      return { binary: true, mime: `image/jpeg` };

    case 'png':
      return { binary: true, mime: `image/png` };

    case 'ogg':
      return { binary: true, mime: `audio/ogg` };

    case 'mp3':
      return { binary: true, mime: `audio/mpeg` };
  }

  throw new Error(`The extension ${ext} is not recognized`);
}
