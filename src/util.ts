/**
 * Throw with a message (used in expression position).
 */
export function throwMsg(...msg: string[]): never {
  console.error('App error:', msg);
  throw new Error(msg.join(' '));
}

/**
 * Generates the classes for a JSX element, where each element is activated with a boolean.
 */
export function classes(cl: { [key: string]: boolean }) {
  return Object.entries(cl)
    .map(([name, active]) => (active ? name : null))
    .filter((name) => name !== null)
    .join(' ');
}

/**
 * The promise resolve either when the audio has finished playing, or when there is an error.
 */
export async function playAudio(audio: HTMLAudioElement): Promise<void> {
  return new Promise((resolve) => {
    audio.addEventListener('ended', () => resolve());
    audio.play().catch((r) => {
      console.error('Cannot read audio', audio, 'for reason', r);
      resolve();
    });
  });
}

/**
 * Update an element in an array given its index:
 */
export function updateElem<T>(array: T[], index: number, f: (elem: T) => T): T[] {
  if (index >= array.length) {
    throw new Error('Cannot update the array: out of bounds');
  }

  return [...array.slice(0, index), f(array[index]), ...array.slice(index + 1)];
}

export async function readAsDataUri(data: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('loadend', (e) => {
      if (typeof reader.result === 'string') {
        resolve(reader.result);
      } else {
        reject(reader.error);
      }
    });
    reader.readAsDataURL(data);
  });
}

export async function promiseAllSettled<T>(
  promises: Promise<T>[],
): Promise<{ fulfilled: T[]; rejected: any[] }> {
  const results = await Promise.allSettled(promises);

  const fulfilled = results
    .filter((result) => result.status === 'fulfilled')
    .map((result) => (result as PromiseFulfilledResult<T>).value);

  const rejected = results
    .filter((result) => result.status === 'rejected')
    .map((result) => (result as PromiseRejectedResult).reason);

  return { fulfilled, rejected };
}
