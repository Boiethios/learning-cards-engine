export interface Config {
  displayTextInSwiper: boolean;
  enableConfigShortcut: boolean;
}

/**
 * A topic description.
 */
export interface TopicDescription {
  picture: string;
  alt: string;
  audio: string;
}

/**
 * A single card, with the picture, picture description and audio.
 */
export interface Card {
  picture: string;
  alt: string;
  audio: string;
}

export interface TopicManifest {
  description: TopicDescription;
  cards: Card[];
}

/**
 * User facing manifest.
 */
export type TomlManifest = {
  topics: {
    name: string;
    picture: String;
    alt: String;
    audio: String;
    cards: { picture: string; alt: string; audio: string }[];
  }[];
};

export type Mode = 'learn' | 'find-game';
